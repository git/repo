/*
 * Javascript functions for gitweb.
 *
 * Copyright (C) 2007, Petr Baudis <pasky@suse.cz>
 * Copyright (C) 2007, Fredrik Kuivinen <frekui@gmail.com>
 * Copyright (C) 2007  Martin Koegler <mkoegler@auto.tuwien.ac.at>
 *
 * This file is licensed under GPLv2.
 */

function getCookie (name)
{
  var name = name + "=";
  var c = document.cookie;
  var p = c.indexOf (name);
  if (p == -1)
    return null;
  c = c.substr (p + name.length, c.length);
  p = c.indexOf (";");
  if (p == -1)
    return c;
  else
    return c.substr (0, p);
}

function insertAfter (elem, node)
{
  if (node.nextSibling)
    node.parentNode.insertBefore (elem, node.nextSibling);
  else
    node.parentNode.appendChild (elem);
}

function createLink (href, linktext)
{
  var l = document.createElement ("a");
  l.appendChild (document.createTextNode (linktext));
  l.href = href;
  return l;
}

function createLinkGroup (href1, basetxt, href2, difftxt)
{
  var l = document.createElement ("span");
  l.appendChild (document.createTextNode (" ("));
  l.appendChild (createLink (href1, basetxt));
  l.appendChild (document.createTextNode (" | "));
  l.appendChild (createLink (href2, difftxt));
  l.appendChild (document.createTextNode (") "));
  return l;
}

function GitRef ()
{
  this.t = null;
  this.h = null;
  this.hb = null;
  this.f = null;
  this.p = null;
  this.ToRef = ToRef;
}

function ToRef ()
{
  var parts = new Array ();
  if (this.f)
    parts.push ("f=" + this.f);
  if (this.h)
    parts.push ("h=" + this.h);
  if (this.hb)
    parts.push ("hb=" + this.hb);
  if (this.t)
    parts.push ("t=" + this.t);
  if (this.p)
    parts.push ("p=" + this.p);
  return parts.join ("@");
}

function splitGitRef (ref)
{
  var parts = ref.split ("@");
  var res = new GitRef ();
  var i;
  for (i = 0; i < parts.length; i++)
    {
      var p = parts[i].split ("=");
      res[p[0]] = p[1];
    }
  return res;
}

function GitURL (base)
{
  this.base = base;
  this.p = null;
  this.a = null;
  this.f = null;
  this.fp = null;
  this.h = null;
  this.hp = null;
  this.hb = null;
  this.hpb = null;
  this.pg = null;
  this.o = null;
  this.s = null;
  this.st = null;
  this.ToURL = ToURL;
  this.ToRef = UrlToRef;
  this.ToDUrl = ToDUrl;
}

function ToURL ()
{
  var parts = new Array ();
  if (this.p)
    parts.push ("p=" + this.p);
  if (this.a)
    parts.push ("a=" + this.a);
  if (this.f)
    parts.push ("f=" + this.f);
  if (this.fp)
    parts.push ("fp=" + this.fp);
  if (this.h)
    parts.push ("h=" + this.h);
  if (this.hp)
    parts.push ("hp=" + this.hp);
  if (this.hb)
    parts.push ("hb=" + this.hb);
  if (this.hpb)
    parts.push ("hpb=" + this.hpb);
  if (this.o)
    parts.push ("o=" + this.o);
  if (this.s)
    parts.push ("s=" + this.s);
  if (this.st)
    parts.push ("st=" + this.st);
  return this.base + "?" + parts.join (";");
}

function UrlToRef (type)
{
  var res = new GitRef;
  res.f = this.f;
  res.h = this.h;
  res.hb = this.hb;
  res.t = type;
  res.p = this.p;
  return res.ToRef ();
}

function ToDUrl (type)
{
  var res = new GitURL (this.base);
  res.f = this.f;
  res.h = this.h;
  res.hb = this.hb;
  res.p = this.p;
  res.a = type;
  return res.ToURL ();
}

function splitGitURL (url)
{
  var Urls = url.split ("?");
  var res = new GitURL (Urls[0]);
  if (Urls.length > 1)
    {
      var parts = Urls[1].split (";");
      var i;
      for (i = 0; i < parts.length; i++)
	{
	  var p = parts[i].split ("=");
	  res[p[0]] = p[1];
	}
    }
  return res;
}

function base (ref)
{
  document.cookie = "basename=" + ref;
}

function diff (url)
{
  var c = getCookie ("basename");
  if (!c)
    {
      alert ("no diff base selected");
      return;
    }
  c = splitGitRef (c);
  url = splitGitURL (url);

  if (c.p != url.p)
    {
      alert ("base object in an other repository");
      return;
    }

  if (c.t == 'commit' && url.a == 'commit')
    {
      url.a = 'commitdiff';
      if (!c.h || !url.h)
	{
	  alert ("commit diff not possible");
	  return;
	}
      url.hb = null;
      url.f = null;
      url.hp = c.h;
      document.location.href = url.ToURL ();
      return;
    }
  if (c.t == 'blob' && url.a == 'blob')
    {
      url.a = 'blobdiff';
      url.hp = c.h;
      url.hpb = c.hb;
      url.fp = c.f;
      document.location.href = url.ToURL ();
      return;
    }
  if (c.t == 'tree' && url.a == 'tree')
    {
      url.a = 'treediff';
      url.hpb = c.hb;
      url.hp = c.h;
      url.fp = c.f;
      document.location.href = url.ToURL ();
      return;
    }
  if (c.t == 'commit' && url.a == 'tree')
    {
      url.a = 'treediff';
      url.hpb = c.h;
      url.hp = null;
      url.fp = null;
      document.location.href = url.ToURL ();
      return;
    }
  if (c.t == 'tree' && url.a == 'commit')
    {
      url.a = 'treediff';
      url.hpb = c.hb;
      url.hp = c.h;
      url.fp = c.f;
      url.hb = url.h;
      url.h = null;
      document.location.href = url.ToURL ();
      return;
    }
  alert ("diff not possible");
}

function GitAddLinks ()
{
  var links = document.getElementsByTagName ("a");
  var i;

  for (i = 0; i < links.length; i++)
    {
      var link = links[i];
      var url = splitGitURL (link.href);
      if (link.innerHTML == 'commit' || link.innerHTML == 'tag')
	{
	  if (!url.h)
	    continue;
	  var l =
	    createLinkGroup ("javascript:base('" + url.ToRef ('commit') +
			     "')", "base",
			     "javascript:diff('" + url.ToDUrl ('commit') +
			     "')", "diff");
	  insertAfter (l, link);
	}
      if (link.innerHTML == 'blob')
	{
	  if (!url.h && !(url.hb && url.f))
	    continue;
	  var l =
	    createLinkGroup ("javascript:base('" + url.ToRef ('blob') + "')",
			     "base",
			     "javascript:diff('" + url.ToDUrl ('blob') + "')",
			     "diff");
	  insertAfter (l, link);
	}
      if (link.innerHTML == 'tree')
	{
	  if (!url.h && !(url.hb && url.f))
	    continue;
	  var l =
	    createLinkGroup ("javascript:base('" + url.ToRef ('tree') + "')",
			     "base",
			     "javascript:diff('" + url.ToDUrl ('tree') + "')",
			     "diff");
	  insertAfter (l, link);
	}
    }


// blame extra columns

// I would like to note here that JavaScript is utterly stupid.
function findStyleRule(styleName) {
	for (i = 0; i < document.styleSheets.length; i++) { 
		// MSIE has .rules, Mozilla has .cssRules
		var cssRules = document.styleSheets[i].cssRules ? document.styleSheets[i].cssRules : document.styleSheets[i].rules;
		for (j = 0; j < cssRules.length; j++) {
			var rule = cssRules[j];
			if (rule.selectorText.toLowerCase() == styleName) {
				return rule;
			}
		}
	}
}

var isIE = (navigator.appName.toLowerCase().indexOf("microsoft") != -1);
var extra_columns = 0;
var extra_column_rule = null;
function extra_blame_columns() {
	if (!extra_column_rule)
		extra_column_rule = findStyleRule(".extra_column");

	if (!extra_columns) {
		document.getElementById("columns_expander").innerHTML = "[-]";
		extra_column_rule.style.display = isIE ? "inline" : "table-cell";
		extra_columns = 1;
	} else {
		document.getElementById("columns_expander").innerHTML = "[+]";
		extra_column_rule.style.display = "none";
		extra_columns = 0;
	}
}


// blame_interactive

var DEBUG = 0;
function debug(str)
{
	if (DEBUG)
		alert(str);
}

function createRequestObject() {
	var ro;
	if (window.XMLHttpRequest) {
		ro = new XMLHttpRequest();
	} else {
		ro = new ActiveXObject("Microsoft.XMLHTTP");
	}
	return ro;
}

var http;
var baseUrl;

// 'commits' is an associative map. It maps SHA1s to Commit objects.
var commits = new Object();

function Commit(sha1)
{
	this.sha1 = sha1;
}

function zeroPad(n)
{
	if (n < 10)
		return '0' + n;
	else
		return n.toString();
}

function handleLine(commit)
{
	/* This is the structure of the HTML fragment we are working
	   with:
	   
	   <tr id="l123" class="light2">
	   <td class="sha1" title="">
	   <a href=""></a>
	   </td>
	   <td class="linenr">
	   <a class="linenr" href="">123</a>
	   </td>
	   <td class="pre"># times (my ext3 doesn&#39;t).</td>
	   </tr>
	 */

	var resline = commit.resline;
	for (var i = 0; i < commit.numlines; i++) {
		var tr = document.getElementById('l'+resline);
		if (!tr) {
			debug('tr is null! resline: ' + resline);
			break;
		}

		var date = new Date();
		date.setTime(commit.authorTime * 1000);
		var dateStr =
			date.getUTCFullYear() + '-'
			+ zeroPad(date.getUTCMonth()+1) + '-'
			+ zeroPad(date.getUTCDate());
		var timeStr =
			zeroPad(date.getUTCHours()) + ':'
			+ zeroPad(date.getUTCMinutes()) + ':'
			+ zeroPad(date.getUTCSeconds());
		tr.firstChild.title = commit.author + ', ' + dateStr + ' ' + timeStr;
		var shaAnchor = tr.firstChild.firstChild;
		var authorField = tr.firstChild.nextSibling;
		var dateField = tr.firstChild.nextSibling.nextSibling;
		if (i == 0) {
			shaAnchor.href = baseUrl + ';a=commit;h=' + commit.sha1;
			shaAnchor.innerHTML = commit.sha1.substr(0, 8);
			authorField.innerHTML = commit.author;
			dateField.innerHTML = dateStr + ' ' + timeStr;
		} else {
			shaAnchor.innerHTML = '';
			authorField.innerHTML = '';
			dateField.innerHTML = '';
		}

		var lineAnchor = tr.firstChild.nextSibling.nextSibling.nextSibling.firstChild;
		lineAnchor.href = baseUrl + ';a=blame;hb=' + commit.sha1
			+ ';f=' + commit.filename + '#l' + commit.srcline;
		resline++;
	}
}

function fixColors()
{
	var colorClasses = ['light2', 'dark2'];
	var linenum = 1;
	var tr;
	var colorClass = 0;

	while ((tr = document.getElementById('l'+linenum))) {
		if (tr.firstChild.firstChild.innerHTML != '') {
			colorClass = (colorClass + 1) % 2;
		}
		tr.setAttribute('class', colorClasses[colorClass]);
		// Internet Explorer needs this
		tr.setAttribute('className', colorClasses[colorClass]);
		linenum++;
	}
}

var prevDataLength = -1;
var nextLine = 0;
var inProgress = false;

var sha1Re = new RegExp('([0-9a-f]{40}) ([0-9]+) ([0-9]+) ([0-9]+)');
var infoRe = new RegExp('([a-z-]+) ?(.*)');
var curCommit = new Commit();

function handleResponse() {
	debug('handleResp ready: ' + http.readyState
	      + ' respText null?: ' + (http.responseText === null)
	      + ' progress: ' + inProgress);

	if (http.readyState != 4 && http.readyState != 3)
		return;

	// In konqueror http.responseText is sometimes null here...
	if (http.responseText === null)
		return;

	if (inProgress)
		return;
	else
		inProgress = true;

	while (prevDataLength != http.responseText.length) {
		if (http.readyState == 4
		    && prevDataLength == http.responseText.length) {
			break;
		}

		prevDataLength = http.responseText.length;
		var response = http.responseText.substring(nextLine);
		var lines = response.split('\n');
		nextLine = nextLine + response.lastIndexOf('\n') + 1;
		if (response[response.length-1] != '\n') {
			lines.pop();
		}

		for (var i = 0; i < lines.length; i++) {
			var match = sha1Re.exec(lines[i]);
			if (match) {
				var sha1 = match[1];
				var srcline = parseInt(match[2]);
				var resline = parseInt(match[3]);
				var numlines = parseInt(match[4]);
				var c = commits[sha1];
				if (!c) {
					c = new Commit(sha1);
					commits[sha1] = c;
				}

				c.srcline = srcline;
				c.resline = resline;
				c.numlines = numlines;
				curCommit = c;
			} else if ((match = infoRe.exec(lines[i]))) {
				var info = match[1];
				var data = match[2];
				if (info == 'filename') {
					curCommit.filename = data;
					handleLine(curCommit);
				} else if (info == 'author') {
					curCommit.author = data;
				} else if (info == 'author-time') {
					curCommit.authorTime = parseInt(data);
				}
			} else if (lines[i] != '') {
				debug('malformed line: ' + lines[i]);
			}
		}
	}

	if (http.readyState == 4 && prevDataLength == http.responseText.length)
		fixColors();

	inProgress = false;
}

function startBlame(blamedataUrl, bUrl)
{
	baseUrl = bUrl;
	http = createRequestObject();
	http.open('get', blamedataUrl);
	http.onreadystatechange = handleResponse;
	http.send(null);
}
